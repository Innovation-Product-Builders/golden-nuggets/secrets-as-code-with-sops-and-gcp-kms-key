# secrets-as-code-with-sops-and-gcp-kms-key
Managing secrets for application deployment is a tricky challenge for both, developer and DevOps teams.

## Vault and Secret Manager Tools
There are a lot of options to securely handle secrets via vaults, secret managers, etc. However, in such cases the applications need to have credentials to talk with these vaults, secret managers, any version control to fetch the required secrets value for the application.

## Secret Management in Repositories
For this reason, we present here another option for secret handling, which is is use the development repositories as storage for **encrypted** secret files. This method ensures that if someone gets access to our repositories, which are typically well protected due to the high-value IP inside them, it requires additionally the appropriate permissions to decrypt the secret files to access the secrets.

## Secrets-as-Code
To realize secrets-as-code we will use Mozilla SOPS and KMS keys (GCP, AWS, AZURE) to encrypt secrets values for any application and push the encrypted secrets to our repository like code, i.e, secrets-as-code. Now the secrets are part of the main application code.
- Here, Mozilla SOPS is the CLI tools to perform the encryption and decryption step.
- KMS keys generated in GCP/AWS/Azure are used as the encryption / decryption keys.

In this way, the Version-Control-System of choice such as GitLab or GitHub becomes the source of truth for all your code and its secrets.

## IAM for proper protection
The key part for the proper access management to the used KMS keys from GCP/AWS/Azure is based on the IAM capabilties of these cloud providers. This means instead of duplicating or maintaining a seperate access management in separate vault / secret manager tool, the IAM capabilities of GCP/AWS/Azure are used to manage the proper access to the encryption / decryption keys.


# Technical Tutorial 1: SPOS and GCP KMS


## Prerequisites
- Install SOPS: https://github.com/getsops/sops
- Intall the `gcloud` CLI tool: https://cloud.google.com/sdk/docs/install
- Login into your GCP project with `gcloud auth login`
- In order for SOPS to be able to make calls to the GCP API the so-called ADCs (application defaul credentials) need to be created with `gcloud auth application-default login`
- Ensure in GCP IAM that the user has appropriate permissions for Cloud KMS CryptoKey Encrypter/Decrypter to make actions with this Key with sops.

## What is Mozilla SOPS?
Mozilla SOPS is a cli tool which works with file types which rely on a key:value format (.json, .yaml, .env). SOPS works with these files by **encrypting only the values**, allowing us to see the keys and thereby comprehend the set of secrets that are present on that file without leaking the values.

Install sops in your PC
- For MAC: `brew install sops`
- For Windows: Just download the .exe from here https://github.com/getsops/sops and put it in a folder on your path

## Generating a GCP KMS key
Key creations via commadline

```
gcloud kms keyrings create sops-test --location global
gcloud kms keys create sops-test-key --location global --keyring sops-test --purpose encryption
gcloud kms keys list --location global --keyring sops-test
```
In case our fictional GCP project is called `gcp-platform-lab`, the output of the resources is:
```
projects/gcp-platform-lab/locations/global/keyRings/sops-test/cryptoKeys/sops-test-key
```

Now we have successfully created a key inside a key-ring.


## Using SOPS with the GCP KMS key
We have plain values of secrets in the directory test/plain-secrets.yaml file. Now we will make this plain secrets values file as secrets-as-code to push it to our version-control-system (VCS).

### Encrypt opertation for the gcp project `gcp-platform-lab`
```
sops --encrypt --gcp-kms  projects/gcp-platform-lab/locations/global/keyRings/sops-test/cryptoKeys/sops-test-key  test/plain-secrets.yaml > test/encrypted-secrets.yaml
```
You will get encrypted secrets which arecreated in the `test/encrypted-secrets.yaml` file from the plain secrets. Now its safe to store the encrypted file of secrets in your VCS without any risk of leaking as it will only accessible for permitted users to decrypt/encrypt.

### Decrypt operation
```
sops --decrypt test/encrypted-secrets.yaml
```

### Modify/edit values in the file
```
sops  test/encrypted-secrets.yaml
```

# Technical Tutorial 1: Secrets-as-Code in Helm-Charts

Now let's suppose we want to deploy a PostgeSQL database with the `bitnami/postgresql` helm-chart in our GKE Kubernetes Cluster to have a self-hosted DB. In this chart we have a long list of values [files](https://github.com/bitnami/charts/blob/main/bitnami/postgresql/values.yaml) where some values must be passed as secrets-as-code in our versioning control system.

We will use SOPS and GCP KMS here to realize the secets-as-code approach for the sensative secrets values inside this chart.

## How to view, update, and create secrets using KMS keys for Helm-Charts

- Install `helm`, the `helm secrets` plugin and `helmfile` on your PC.
- Now we will use `helm secrets` plugin to encrypt data by using the previosly generated KMS key stored in **.sops.yaml** file.

Create secrets file(secrests.yaml) from a plain values file(plain-secret.yaml)
```
cd postgres
# create a plain-secret.yaml in values/ directory and put all secret key-value in plain text and below command will create encrypted secrets.yaml file
helm secrets encrypt values/plain-secret.yaml > values/secrets.yaml
rm -rf values/plain-secret.yaml
```
View secrets file values as plain-text

```
cd postgres
helm secrets decrypt  values/secrets.yaml
```
Update secrets file(add new key-value or update existing value)

```
cd postgres
helm secrets edit  values/secrets.yaml
# press i to activate input session
# add or update desired values
# press esc button to disable input session
# press :wq to save and quit
```
Now push your code with encrypted secrets as code in VCS without having concern any security leak issue.

## Deploy DB application on local kubernetes

We are using helmfile to deploy helmcharts

```
cd postgres
helmfile sync
```
It will automatically pick secrets values from values/secrets.yaml to set postgresPassword value for the helm release.
